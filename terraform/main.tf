provider "aws" {
  #access_key = ""
  #secret_key = ""
  #region = ""
}

/*
# Добавляем публичный ключ в AWS
resource "aws_key_pair" "user" {
  key_name   = "user"
  public_key = "${file("/home/user/.ssh/id_rsa.pub")}"
}
*/

# Заводим security group для нашего будущего инстанса лоадбалансера и разрешаем входящие
#  соединения на 22, 80 и 443 портах
resource "aws_security_group" "loadbalancer" {
  name = "loadbalancer"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all ouput traffic"
  }
}

# Заводим security group для наших будущих инстанса бэкэнда и разрешаем входящие
#  соединения на 22, 80 и 443 портах
resource "aws_security_group" "backend" {
  name = "backend"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all ouput traffic"
  }
}

# Заводим наш инстанс лоадбалансера с возможностью простого provision'инга.
# Провиженить можно и другими спомобами, например использовать Chef или Ansible
resource "aws_instance" "inst1" {
  ami             = "ami-0d5d9d301c853a04a"
  instance_type   = "t2.micro"
  key_name        = "id_rsa"
  security_groups = ["${aws_security_group.loadbalancer.id}"]
  subnet_id       = "subnet-9c0304f4" # Public subnet
  #  user_data         = "${file("user_data.sh")}"

  tags = {
    Name  = "inst1-loadbalancer"
    Owner = "Andrey Polyakov"
  }

  connection {
    host    = "${aws_instance.inst1.public_ip}"
    user    = "ubuntu"
    agent   = true
    timeout = "5m"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      /*
      "sudo apt -y install apt-transport-https ca-certificates curl software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable'",
      "sudo apt update",
      "sudo apt -y install docker-ce",
      "sudo mkdir inst1",
      */
    ]
    #zq
  }
  #depends_on = ["aws_key_pair.user"]
}

# Заводим наш инстанс бэкэнда с возможностью простого provision'инга.
# Провиженить можно и другими спомобами, например использовать Chef или Ansible
resource "aws_instance" "inst2" {
  ami             = "ami-0d5d9d301c853a04a"
  instance_type   = "t2.micro"
  key_name        = "id_rsa"
  security_groups = ["${aws_security_group.backend.id}"]
  subnet_id       = "subnet-85b9c4ff" # Privat1 subnet
  user_data       = "${file("user_data.sh")}"

  tags = {
    Name  = "inst2-backend"
    Owner = "Andrey Polyakov"
  }
  /*
  connection {
    host    = "${aws_instance.inst2.public_ip}"
    user    = "ubuntu"
    agent   = true
    timeout = "5m"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
    ]
  }
*/
}

# Заводим наш инстанс бэкэнда с возможностью простого provision'инга.
# Провиженить можно и другими спомобами, например использовать Chef или Ansible
resource "aws_instance" "inst3" {
  ami             = "ami-0d5d9d301c853a04a"
  instance_type   = "t2.micro"
  key_name        = "id_rsa"
  security_groups = ["${aws_security_group.backend.id}"]
  subnet_id       = "subnet-85b9c4ff" # Privat1 subnet
  user_data       = "${file("user_data.sh")}"

  tags = {
    Name  = "inst3-backend"
    Owner = "Andrey Polyakov"
  }
}

# Заводим наш инстанс бэкэнда базы данных с возможностью простого provision'инга.
# Провиженить можно и другими спомобами, например использовать Chef или Ansible
resource "aws_instance" "inst4" {
  ami             = "ami-0d5d9d301c853a04a"
  instance_type   = "t2.micro"
  key_name        = "id_rsa"
  security_groups = ["${aws_security_group.backend.id}"]
  subnet_id       = "subnet-85b9c4ff" # Privat1 subnet
  user_data       = "${file("user_data.sh")}"

  tags = {
    Name  = "inst4-db"
    Owner = "Andrey Polyakov"
  }
}

# На последок опишем ту информацию, которую хотим знать о рабочей инфраструктуре.
# В данном случае - это ip адреса наших инстансов.
output "inst1_ip_loadbalancer" {
  value = "${aws_instance.inst1.public_ip}"
}

output "inst2_ip_backend" {
  value = "${aws_instance.inst2.public_ip}"
}

output "inst3_ip_backend" {
  value = "${aws_instance.inst3.public_ip}"
}

output "inst4_ip_db" {
  value = "${aws_instance.inst4.public_ip}"
}
